﻿using SpecFlowProjectCalculator.Drivers;
using TechTalk.SpecFlow;

namespace SpecFlowProjectCalculator.Hooks
{
    [Binding]
    public sealed class CalculatorHooks
    {
        private readonly CalculatorDriver _calculatorDriver = new CalculatorDriver();

        [BeforeScenario]
        public void BeforeScenario()
        {
            _calculatorDriver.LaunchCalculator();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            _calculatorDriver.CloseCalculator();
        }
    }
}
