﻿Feature: CalculatorDateMode
	Validate the days & weeks results for the given date range

@DateMode
Scenario: Calculator With Date Mode
	Given navigate to Date mode
	And the first date selected as 6
	And the second date selected as 25
	Then the difference in weeks should be 2 weeks, 5 days
	And the difference in days should be 19 days