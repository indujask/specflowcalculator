﻿Feature: CalculatorProgrammerMode
	Validate the results on hex, dec, bin & oct formats for the given number

@ProgrammerMode
Scenario: Calculator With Programmer Mode
	Given navigate to programmer mode
	And enter number 3
	And enter number 2
	Then the hex result should be 20
	And the dec result should be 32
	And the oct result should be 40
	And the bin result should be 0010 0000