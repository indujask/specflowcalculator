﻿Feature: CalculatorScientificMode
	Validate the square of value for the given number

@ScientificMode
Scenario: Calculator With Scientific Mode
	Given navigate to scientific mode
	And enter number 9
	When the square of clicked
	Then the output should be 81