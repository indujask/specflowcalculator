﻿Feature: CalculatorStandardMode
	Validate the calculated results for the given numbers

@StandardMode
Scenario: Calculator With Standard Mode
	Given navigate to Standard mode
	And the number entered is 99
	And the multiply icon clicked
	And the number entered is 25
	When the user hits equals icon
	Then the result should be 2,475
	When the result is clearned on cancel
	Then the result should be 0
	Given the number entered is 40
	And the divide icon clicked
	And the number entered is 20
	When the user hits equals icon
	Then the result should be 2
	When the result is clearned on cancel
	Then the result should be 0
	Given navigate to history tab
	When the user restores the result from first available history
	Then the result should be 2
	When the user restores the result from second available history
	Then the result should be 2,475
	When the result is clearned on cancel
	Then the result should be 0
	Given navigate to memory tab
	And the number entered is 27
	And the divide icon clicked
	And the number entered is 2
	When the user hits equals icon
	And the memory add clicked
	Then the result should be 13.5	
	When the result is clearned on cancel
	Then the result should be 0
	When the user restores the result from memory list view
	Then the result should be 13.5
	When the result is clearned on cancel
	Then the result should be 0
	Given the number entered is 5
	And the memory minus icon clicked
	When the user restores the result from memory recall button
	Then the result should be 8.5