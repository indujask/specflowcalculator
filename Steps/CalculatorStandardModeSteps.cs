﻿using SpecFlowProjectCalculator.Drivers;
using TechTalk.SpecFlow;

namespace SpecFlowProjectCalculator.Steps
{
    [Binding]
    public class CalculatorStandardModeSteps
    {
        private readonly CalculatorDriver calculatorDriver = new CalculatorDriver();

        [Given(@"navigate to Standard mode")]
        public void GivenNavigateToStandardMode()
        {
            calculatorDriver.ToggleCalculatorMode("Standard");
        }

        [Given("the number entered is (.*)")]
        public void GivenTheNumberEntered(string number)
        {
            calculatorDriver.EnterNumber(number);
        }

        [Given(@"the multiply icon clicked")]
        public void GivenTheMultiplyIconClicked()
        {
            calculatorDriver.ClickMutltiplyIcon();
        }

        [Given(@"the divide icon clicked")]
        public void GivenTheDivideIconClicked()
        {
            calculatorDriver.ClickDivideIcon();
        }
        
        [When(@"the user hits equals icon")]
        public void WhenTheUserHitsEqualsIcon()
        {
            calculatorDriver.ClickEqualIcon();
        }

        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(string result)
        {
            calculatorDriver.ValidateResult(result);
        }

        [When(@"the result is clearned on cancel")]
        public void WhenTheResultIsClearnedOnCancel()
        {
            calculatorDriver.ClearResults();
        }
        
        [When(@"the user restores the result from first available history")]
        public void WhenTheUserRestoresTheResultFromFirstAvailableHistory()
        {
            calculatorDriver.RestoreFromHistory(0);
        }
        
        [When(@"the user restores the result from second available history")]
        public void WhenTheUserRestoresTheResultFromSecondAvailableHistory()
        {
            calculatorDriver.RestoreFromHistory(1);
        }

        [Given("navigate to history tab")]
        public void GivenNavigateToHistoryTab()
        {
            calculatorDriver.ClickHistoryTab();
        }

        [Given("navigate to memory tab")]
        public void GivenNavigateToMemoryTab()
        {
            calculatorDriver.ClickMemoryTab();
        }

        [When(@"the memory add clicked")]
        public void WhenTheMemoryAddClicked()
        {
            calculatorDriver.ClickMemoryPlus();
        }

        [When(@"the user restores the result from memory list view")]
        public void WhenTheUserRestoresTheResultFromMemoryListView()
        {
            calculatorDriver.RestoreFromMemoryListView();
        }

        [When(@"the user restores the result from memory recall button")]
        public void WhenTheUserRestoresTheResultFromMemoryRecallButton()
        {
            calculatorDriver.RestoreFromMemoryRecallButton();
        }

        [Given(@"the memory minus icon clicked")]
        public void GivenTheMemoryMinusIconClicked()
        {
            calculatorDriver.ClickMemoryMinus();
        }

    }
}
