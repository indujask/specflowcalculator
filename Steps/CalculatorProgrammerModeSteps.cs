﻿using SpecFlowProjectCalculator.Drivers;
using TechTalk.SpecFlow;

namespace SpecFlowProjectCalculator.Steps
{
    [Binding]
    public class CalculatorProgrammerModeSteps
    {
        private readonly CalculatorDriver calculatorDriver = new CalculatorDriver();

        [Given("navigate to programmer mode")]
        public void GivenNavigateToProgrammerMode()
        {
            calculatorDriver.ToggleCalculatorMode("Programmer");
        }
        
        [Given("enter number (.*)")]
        public void GivenEnterNumber(string number)
        {
            calculatorDriver.EnterNumber(number);
        }
        
        [Then("the hex result should be (.*)")]
        public void ThenTheHexResultShouldBe(int hexResult)
        {
            calculatorDriver.Programmer_ValidateHexResult(hexResult);
        }

        [Then("the dec result should be (.*)")]
        public void ThenTheDecResultShouldBe(int decResult)
        {
            calculatorDriver.Programmer_ValidateDecResult(decResult);
        }

        [Then("the oct result should be (.*)")]
        public void ThenTheOctResultShouldBe(int octResult)
        {
            calculatorDriver.Programmer_ValidateOctResult(octResult);
        }

        [Then("the bin result should be (.*)")]
        public void ThenTheBinResultShouldBe(string binResult)
        {
            calculatorDriver.Programmer_ValidateBinResult(binResult);
        }
    }
}
