﻿using SpecFlowProjectCalculator.Drivers;
using TechTalk.SpecFlow;

namespace SpecFlowProjectCalculator.Steps
{
    [Binding]
    public class CalculatorScientificModeSteps
    {
        private readonly CalculatorDriver calculatorDriver = new CalculatorDriver();

        [Given(@"navigate to scientific mode")]
        public void GivenNavigateToScientificMode()
        {
            calculatorDriver.ToggleCalculatorMode("Scientific");
        }
        
        [When(@"the square of clicked")]
        public void WhenTheSquareOfClicked()
        {
            calculatorDriver.ClickXPowerofTwo();
        }

        [Then(@"the output should be (.*)")]
        public void ThenTheOutputShouldBe(int result)
        {
            calculatorDriver.ValidateXPowerOfTwo(result);
        }
    }
}
