﻿using SpecFlowProjectCalculator.Drivers;
using TechTalk.SpecFlow;

namespace SpecFlowProjectCalculator.Steps
{
    [Binding]
    public class CalculatorDateModeSteps
    {
        private readonly CalculatorDriver calculatorDriver = new CalculatorDriver();

        [Given(@"navigate to Date mode")]
        public void GivenNavigateToDateMode()
        {
            calculatorDriver.ToggleCalculatorMode("Date");
        }
        
        [Given(@"the first date selected as (.*)")]
        public void GivenTheFirstDateSelectedAs(string fromDate)
        {
            calculatorDriver.SelectFromDate(fromDate);
        }
        
        [Given(@"the second date selected as (.*)")]
        public void GivenTheSecondDateSelectedAs(string toDate)
        {
            calculatorDriver.SelectToDate(toDate);
        }
        
        [Then(@"the difference in weeks should be (.*)")]
        public void ThenTheDifferenceInWeeksShouldBe(string dateDiffInWeeks)
        {
            calculatorDriver.ValidateDateDifferenceInWeeks(dateDiffInWeeks);
        }

        [Then(@"the difference in days should be (.*)")]
        public void ThenTheDifferenceInDayshouldBe(string dateDiffInDays)
        {
            calculatorDriver.ValidateDateDifferenceInDays(dateDiffInDays);
        }
    }
}
