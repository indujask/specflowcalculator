﻿using FluentAssertions;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows.Automation;
using System.Windows.Forms;

namespace SpecFlowProjectCalculator.Drivers
{
    public class CalculatorDriver
    {
        public UiaUtils utils = new UiaUtils();

        #region Automation Conditions

        private readonly Condition Condition_Calculator_Window = new PropertyCondition(AutomationElement.NameProperty, "Calculator");
        private readonly Condition Condition_Calculator_MinimizeButton = new AndCondition(new PropertyCondition(AutomationElement.AutomationIdProperty, "Maximize"), new PropertyCondition(AutomationElement.NameProperty, "Restore Calculator"));
        private readonly Condition Condition_Calculator_MaximizeButton = new AndCondition(new PropertyCondition(AutomationElement.AutomationIdProperty, "Maximize"), new PropertyCondition(AutomationElement.NameProperty, "Maximize Calculator"));
        private readonly Condition Condition_Calculator_ToggleButton = new PropertyCondition(AutomationElement.AutomationIdProperty, "TogglePaneButton");              
        private readonly Condition Condition_Calculator_Results = new PropertyCondition(AutomationElement.AutomationIdProperty, "CalculatorResults");        

        // Automation Conditions - Programmer
        private readonly Condition Condition_Calculator_Programmer = new PropertyCondition(AutomationElement.AutomationIdProperty, "Programmer");
        private readonly Condition Condition_Programmer_hexButton = new PropertyCondition(AutomationElement.AutomationIdProperty, "hexButton");
        private readonly Condition Condition_Programmer_decimalButton = new PropertyCondition(AutomationElement.AutomationIdProperty, "decimalButton");
        private readonly Condition Condition_Programmer_octolButton = new PropertyCondition(AutomationElement.AutomationIdProperty, "octolButton");
        private readonly Condition Condition_Programmer_binaryButton = new PropertyCondition(AutomationElement.AutomationIdProperty, "binaryButton");

        // Automation Conditions - Scientific
        private readonly Condition Condition_Calculator_Scientific = new PropertyCondition(AutomationElement.AutomationIdProperty, "Scientific");
        private readonly Condition Condition_Scientific_XPowerOfTwoButton = new PropertyCondition(AutomationElement.AutomationIdProperty, "xpower2Button");

        // Automation Conditions - Date
        private readonly Condition Condition_Calculator_DateCalculation = new PropertyCondition(AutomationElement.AutomationIdProperty, "Date");
        private readonly Condition Condition_DateCalculation_FromDate = new PropertyCondition(AutomationElement.AutomationIdProperty, "DateDiff_FromDate");
        private readonly Condition Condition_DateCalculation_ToDate = new PropertyCondition(AutomationElement.AutomationIdProperty, "DateDiff_ToDate");
        private readonly Condition Condition_DateCalculation_DateDifference = new PropertyCondition(AutomationElement.AutomationIdProperty, "DateDiffAllUnitsResultLabel");
        private readonly Condition Condition_DateCalculation_Grid = new PropertyCondition(AutomationElement.AutomationIdProperty, "DateCalculatorGrid");

        // Automation Conditions - Standard
        private readonly Condition Condition_Calculator_Standard = new PropertyCondition(AutomationElement.AutomationIdProperty, "Standard");
        private readonly Condition Condition_Calculator_Standard_Multiply = new PropertyCondition(AutomationElement.AutomationIdProperty, "multiplyButton");
        private readonly Condition Condition_Calculator_Standard_Divide = new PropertyCondition(AutomationElement.AutomationIdProperty, "divideButton");
        private readonly Condition Condition_Calculator_Standard_Equal = new PropertyCondition(AutomationElement.AutomationIdProperty, "equalButton");
        private readonly Condition Condition_Calculator_Standard_Clear = new PropertyCondition(AutomationElement.AutomationIdProperty, "clearButton");
        private readonly Condition Condition_Calculator_Standard_History = new PropertyCondition(AutomationElement.AutomationIdProperty, "HistoryButton"); 
        private readonly Condition Condition_Calculator_Standard_HistoryList = new PropertyCondition(AutomationElement.AutomationIdProperty, "HistoryListView");
        private readonly Condition Condition_Calculator_Standard_HistoryLabel = new PropertyCondition(AutomationElement.AutomationIdProperty, "HistoryLabel");
        private readonly Condition Condition_Calculator_Standard_Memory = new PropertyCondition(AutomationElement.AutomationIdProperty, "MemoryLabel");
        private readonly Condition Condition_Calculator_Standard_MemoryPlus = new PropertyCondition(AutomationElement.AutomationIdProperty, "MemPlus");
        private readonly Condition Condition_Calculator_Standard_MemoryMinus = new PropertyCondition(AutomationElement.AutomationIdProperty, "MemMinus");
        private readonly Condition Condition_Calculator_Standard_MemoryList = new PropertyCondition(AutomationElement.AutomationIdProperty, "MemoryListView");
        private readonly Condition Condition_Calculator_Standard_MemoryRecall = new PropertyCondition(AutomationElement.AutomationIdProperty, "MemRecall"); 

        #endregion

        #region Automation Elements

        private AutomationElement CalculatorWindow => AutomationElement.RootElement.FindFirst(TreeScope.Children, Condition_Calculator_Window);
        private AutomationElement MaximizeButton => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_MaximizeButton);
        private AutomationElement RestoreButton => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_MinimizeButton);
        private AutomationElement ToggleButton => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_ToggleButton);       
        private AutomationElement CalculatorResults => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_Results);                

        // Automation Elements - Programmer Mode
        private AutomationElement CalculatorProgrammer => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_Programmer);
        private AutomationElement CalculatorProgrammer_Hex => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Programmer_hexButton);
        private AutomationElement CalculatorProgrammer_Oct => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Programmer_octolButton);
        private AutomationElement CalculatorProgrammer_Dec => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Programmer_decimalButton);
        private AutomationElement CalculatorProgrammer_Bin => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Programmer_binaryButton);

        // Automation Elements - Scientific Mode
        private AutomationElement CalculatorScientific => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_Scientific);
        private AutomationElement CalculatorScientific_XPowerOfTwoButton => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Scientific_XPowerOfTwoButton);

        // Automation Elements - Date Mode
        private AutomationElement CalculatorDateCalculation => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_DateCalculation);
        private AutomationElement CalculatorDateCalculation_FromDate => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_DateCalculation_FromDate);
        private AutomationElement CalculatorDateCalculation_ToDate => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_DateCalculation_ToDate);
        private AutomationElement CalculatorDateCalculation_DateDifference => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_DateCalculation_DateDifference);
        private AutomationElement CalculatorDateCalculation_DateDifference_Grid => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_DateCalculation_Grid);

        // Automation Elements - Standard Mode
        private AutomationElement CalculatorStandard => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_Standard);
        private AutomationElement CalculatorStandard_Multiply => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_Standard_Multiply);
        private AutomationElement CalculatorStandard_Divide => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_Standard_Divide);
        private AutomationElement CalculatorStandard_Equal => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_Standard_Equal);
        private AutomationElement CalculatorStandard_Clear => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_Standard_Clear);
        private AutomationElement CalculatorStandard_History => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_Standard_HistoryLabel);
        private AutomationElement CalculatorStandard_HistoryList => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_Standard_HistoryList);
        private AutomationElement CalculatorStandard_Memory => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_Standard_Memory);
        private AutomationElement CalculatorStandard_MemoryPlus => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_Standard_MemoryPlus);
        private AutomationElement CalculatorStandard_MemoryMinus => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_Standard_MemoryMinus);
        private AutomationElement CalculatorStandard_MemoryList => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_Standard_MemoryList);
        private AutomationElement CalculatorStandard_MemoryRecall => CalculatorWindow.FindFirst(TreeScope.Descendants, Condition_Calculator_Standard_MemoryRecall); 

        #endregion

        public void LaunchCalculator()
        {
            CloseCalculator();

            Process.Start("Calc.exe");            

            MaximizeCalculator();

            Thread.Sleep(1000);
        }

        public void CloseCalculator()
        {
            var processList = Process.GetProcessesByName("Calculator");

            foreach(var process in processList)
            {
                process.Kill();
            }

            Thread.Sleep(1000);
        }

        public void MaximizeCalculator()
        {
            utils.Uia_WaitForWindowPatternToExist(Condition_Calculator_Window);
            
            if (utils.Uia_IsElementPresent(MaximizeButton))
            {
                utils.Uia_ClickButtonElement(MaximizeButton);
            }
        }

        public void ResetCalculatorMode()
        {
            utils.Uia_WaitForWindowPatternToExist(Condition_Calculator_Window);

            utils.Uia_ClickButtonElement(ToggleButton);

            utils.Uia_ClickButtonElement(CalculatorStandard);
        }

        public void ToggleCalculatorMode(string mode)
        {
            utils.Uia_WaitForWindowPatternToExist(Condition_Calculator_Window);

            utils.Uia_ClickButtonElement(ToggleButton);

            switch (mode)
            {
                case "Programmer":
                    utils.Uia_ClickButtonElement(CalculatorProgrammer);
                    break;
                case "Scientific":
                    utils.Uia_ClickButtonElement(CalculatorScientific);
                    break;
                case "Date":
                    utils.Uia_ClickButtonElement(CalculatorDateCalculation);
                    break;
                case "Standard":
                    utils.Uia_ClickButtonElement(CalculatorStandard);
                    break;
            }
        }

        public void EnterNumber(string number)
        {
            SendKeys.SendWait(number);           
        }

        #region Programmer Mode Validation

        public void Programmer_ValidateHexResult(int hexResult)
        {
            int.Parse(TreeWalker.RawViewWalker.GetFirstChild(CalculatorProgrammer_Hex).Current.Name).Should().Be(hexResult);
        }

        public void Programmer_ValidateDecResult(int decResult)
        {
            int.Parse(TreeWalker.RawViewWalker.GetFirstChild(CalculatorProgrammer_Dec).Current.Name).Should().Be(decResult);
        }

        public void Programmer_ValidateOctResult(int octResult)
        {
            int.Parse(TreeWalker.RawViewWalker.GetFirstChild(CalculatorProgrammer_Oct).Current.Name).Should().Be(octResult);
        }

        public void Programmer_ValidateBinResult(string binResult)
        {
            TreeWalker.RawViewWalker.GetFirstChild(CalculatorProgrammer_Bin).Current.Name.Should().Be(binResult);
        }

        #endregion

        #region Scientific Mode Validation

        public void ClickXPowerofTwo()
        {
            utils.Uia_ClickButtonElement(CalculatorScientific_XPowerOfTwoButton);
        }

        public void ValidateXPowerOfTwo(int result)
        {
            int.Parse(TreeWalker.RawViewWalker.GetFirstChild(CalculatorResults).Current.Name).Should().Be(result);
        }

        #endregion

        #region Date Mode Validation

        public void SelectFromDate(string number)
        {
            utils.Uia_ClickButtonElement(CalculatorDateCalculation_FromDate);

            AutomationElementCollection fromDateCollection = CalculatorWindow.FindAll(TreeScope.Descendants, new PropertyCondition(AutomationElement.NameProperty, number));

            foreach(AutomationElement element in fromDateCollection)
            {
                if (element.Current.IsOffscreen == false)
                {
                    utils.Uia_ClickSelectItemElement(element);
                    break;
                }                
            }
        }

        public void SelectToDate(string number)
        {
            utils.Uia_ClickButtonElement(CalculatorDateCalculation_ToDate);

            AutomationElementCollection toDateCollection = CalculatorWindow.FindAll(TreeScope.Descendants, new PropertyCondition(AutomationElement.NameProperty, number));

            foreach (AutomationElement element in toDateCollection)
            {
                if (element.Current.IsOffscreen == false)
                {
                    utils.Uia_ClickSelectItemElement(element);
                    break;
                }
            }
        }

        public void ValidateDateDifferenceInWeeks(string dateDiffInWeeks)
        {
            var actualDateDifference = CalculatorDateCalculation_DateDifference.Current.Name.Substring(11);

            actualDateDifference.Should().Be(dateDiffInWeeks);            
        }

        public void ValidateDateDifferenceInDays(string dateDiffInDays)
        {
            TreeWalker.RawViewWalker.GetLastChild(CalculatorDateCalculation_DateDifference_Grid).Current.Name.Should().Be(dateDiffInDays);
        }

        #endregion

        #region Standard Mode Validation

        public void ClickMutltiplyIcon()
        {
            utils.Uia_ClickButtonElement(CalculatorStandard_Multiply);
        }

        public void ClickDivideIcon()
        {
            utils.Uia_ClickButtonElement(CalculatorStandard_Divide);
        }

        public void ClickEqualIcon()
        {
            utils.Uia_ClickButtonElement(CalculatorStandard_Equal);
        }

        public void ValidateResult(string result)
        {
            CalculatorResults.Current.Name.Substring(11).Should().Be(result);
        }

        public void ClearResults()
        {
            utils.Uia_ClickButtonElement(CalculatorStandard_Clear);
        }

        public void ClickMemoryTab()
        {
            utils.Uia_ClickSelectItemElement(CalculatorStandard_Memory);
        }

        public void ClickMemoryPlus()
        {
            utils.Uia_ClickButtonElement(CalculatorStandard_MemoryPlus);
        }

        public void ClickMemoryMinus()
        {
            utils.Uia_ClickButtonElement(CalculatorStandard_MemoryMinus);
        }

        public void RestoreFromMemoryListView()
        {
            List<AutomationElement> children = new List<AutomationElement>();

            AutomationElement child = TreeWalker.RawViewWalker.GetFirstChild(CalculatorStandard_MemoryList);

            while (child != null)
            {
                children.Add(child);
                child = TreeWalker.RawViewWalker.GetNextSibling(child);
            }

            if (children.Count > 0)
            {
                utils.Uia_ClickButtonElement(children[0]);
            }
        }

        public void RestoreFromMemoryRecallButton()
        {
            utils.Uia_ClickButtonElement(CalculatorStandard_MemoryRecall);
        }

        public void ClickHistoryTab()
        {
            utils.Uia_ClickSelectItemElement(CalculatorStandard_History);
        }

        public void RestoreFromHistory(int index)
        {
            List<AutomationElement> children = new List<AutomationElement>();

            AutomationElement child = TreeWalker.RawViewWalker.GetFirstChild(CalculatorStandard_HistoryList);

            while (child != null)
            {
                children.Add(child);
                child = TreeWalker.RawViewWalker.GetNextSibling(child);
            }

            if (children.Count > 0)
            {
                utils.Uia_ClickButtonElement(children[index]);
            }
        }

        #endregion
    }
}
