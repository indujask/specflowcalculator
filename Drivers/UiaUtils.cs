﻿using System;
using System.Threading;
using System.Windows.Automation;

namespace SpecFlowProjectCalculator.Drivers
{
    public class UiaUtils
    {
        public void Uia_WaitForWindowPatternToExist(Condition windowCondition)
        {
            AutomationElement windowElement = null;
            int counter = 0;
            int timeout = 50;
            do
            {
                try
                {
                    counter++;
                    windowElement = AutomationElement.RootElement.FindFirst(TreeScope.Children, windowCondition);
                    bool isWindowPropertyAvailable =
                        (bool)windowElement.GetCurrentPropertyValue(AutomationElement
                            .IsWindowPatternAvailableProperty);

                    if (isWindowPropertyAvailable)
                    {
                        if (windowElement.GetCurrentPattern(WindowPattern.Pattern) is WindowPattern windowPattern)
                            break;
                    }

                    if (windowElement != null)
                        break;
                }
                catch (Exception)
                {
                    Thread.Sleep(6000);
                }
            } while (counter < timeout); // Default wait time is 5 minutes

            if (windowElement == null || counter >= timeout)
            {
                throw new ElementNotAvailableException();
            }
        }

        public void Uia_WaitForElementToPresent(AutomationElement element, int timeout = 10)
        {
            // Max it can wait upto 1 minute
            int counter = 0;
            do
            {
                counter++;
                if (element != null && !element.Current.IsOffscreen && element.Current.IsEnabled)
                {
                    break;
                }
                else
                {
                    Thread.Sleep(6000);
                }
            } while (counter < timeout);

            if (counter >= timeout || element == null)
            {
                throw new ElementNotAvailableException();
            }
        }

        public bool Uia_IsElementPresent(AutomationElement element, int timeout = 10)
        {
            // Max it can wait upto 1 minute
            int counter = 0;
            if (element == null)
            {
                return false;
            }

            do
            {
                counter++;
                if (element != null && !element.Current.IsOffscreen && element.Current.IsEnabled)
                {
                    return true;
                }
                else
                {
                    Thread.Sleep(6000);
                }
            } while (counter < timeout);

            return false;
        }

        public void Uia_ClickButtonElement(AutomationElement element)
        {
            int timeout = 10;
            Uia_WaitForElementToPresent(element);
            int counter = 0;
            do
            {
                if (element != null && element.Current.IsEnabled)
                {
                    if (!element.Current.IsOffscreen)
                    {
                        InvokePattern pattern = GetInvokePattern(element);
                        if (pattern != null)
                        {
                            pattern.Invoke();
                            Thread.Sleep(1000);
                            break;
                        }
                    }
                    else
                    {
                        Thread.Sleep(6000);
                    }
                }
                else
                {
                    Thread.Sleep(6000);
                }
            } while (counter < timeout); // Max. it can wait for 1 minute

            if (counter >= timeout || element == null)
            {
                throw new ElementNotAvailableException();
            }
        }

        public void Uia_ClickSelectItemElement(AutomationElement element)
        {
            int timeout = 10;
            Uia_WaitForElementToPresent(element);
            int counter = 0;
            do
            {
                if (element != null && element.Current.IsEnabled)
                {
                    if (!element.Current.IsOffscreen)
                    {
                        SelectionItemPattern pattern = GetSelectionItemPattern(element);
                        if (pattern != null)
                        {
                            pattern.Select();
                            Thread.Sleep(1000);
                            break;
                        }
                    }
                    else
                    {
                        Thread.Sleep(6000);
                    }
                }
                else
                {
                    Thread.Sleep(6000);
                }
            } while (counter < timeout); // Max. it can wait for 1 minute

            if (counter >= timeout || element == null)
            {
                throw new ElementNotAvailableException();
            }
        }

        public InvokePattern GetInvokePattern(AutomationElement element)
        {
            InvokePattern invokePattern;
            try
            {
                invokePattern = element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                Thread.Sleep(1000);
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException();
            }
            catch (Exception e)
            {
                throw e;
            }
            return invokePattern;
        }

        public SelectionItemPattern GetSelectionItemPattern(AutomationElement element)
        {
            SelectionItemPattern invokePattern;
            try
            {
                invokePattern = element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
                Thread.Sleep(1000);
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException();
            }
            catch (Exception e)
            {
                throw e;
            }
            return invokePattern;
        }
    }
}
